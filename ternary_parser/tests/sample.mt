run(`
if (var_1 == 2, 0, if (var_2 == 4, 15, 0))
+ if (var_2 == 3, 5, 0)
- if (var_4 == 2, 0, 5)
+ if (var_3 == 3, 5, 0)`, {
  var_1: 1,
  var_2: 4,
  var_3: 3,
  var_4: 5
});

/*
 output : 15
*/