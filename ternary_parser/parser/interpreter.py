"""
This is contains classes to obtain the semantics of the AST.

The algorithm used is recursive descent parsing

"""
from collections import deque
import copy
from dataclasses import dataclass, field
import operator
from typing import Any

from ternary_parser.grammar.grammar import (
    BINARY_OPERATORS, END, KEYWORDS, NON_TERMINAL, TERMINAL
)
from ternary_parser.parser.parser import Token

OPERATORS = {
    '+' : operator.add,
    '-' : operator.sub,
    '*' : operator.mul,
    '/' : operator.truediv,
    '==': operator.eq,
    '<=': operator.le,
    '>=': operator.ge,
    '<': operator.lt,
    '>': operator.gt
}

class SemanticsError(Exception):
    pass

@dataclass
class Node:
    name: str
    value: Any
    operator: Any = field(default="null")
    is_final: bool = field(default=True)

class ASTVisitor:
    def __init__(self, ast):
        self.AST = copy.deepcopy(ast)
        self.ast_index = 0

        self.var_index = 1
        self.program_vars = {}

        self.stmt_outputs = deque()
        
        self.intermediate_code = deque()

        self.function_mappings = {
            "PROGRAM": self.visit_program,
            "<stmt-list>": self.visit_stmt_list,
            "STMT": self.visit_stmt,
            "<declarations>": self.visit_declarations,
            "<var-dec>": self.visit_var_dec,
            "<more-var-dec>": self.visit_more_var_dec,
            "EXP": self.visit_exp,
            "<ternary>": self.visit_ternary,
            "<condition>": self.visit_exp,
            "<truthy>": self.visit_exp,
            "<falsey>": self.visit_exp,
            "EXP2": self.visit_exp2,
            "NAME": self.visit_name,
            "NUMBER": self.visit_number,
            "DEFAULT": self.move_to_next,
        }

    def traverse(self):
        node = Node(f"temp_var_{self.var_index}", 0)
        tree_node = self.get_tree_node(self.ast_index)
        if tree_node == -1:
            return node
        evaluate_program = self.find_function(tree_node)
        node = evaluate_program()
        return node

    def get_tree_node(self, index):
        if self.ast_index >= len(self.AST):
            return -1
        return self.AST[index]

    def move_to_next(self, step=1, position=None):
        node = Node(f"temp_var_{self.var_index}", 0)
        self.increment_var_index()
        if position != None:
            while self.ast_index < len(self.AST):
                tree_node = self.AST[self.ast_index]
                if tree_node == position:
                    return node
                self.ast_index += 1
        else:
            self.ast_index += step
        return node

    def increment_var_index(self):
        self.var_index += 1
        return

    def find_function(self, tree_node):
        """finds the function we are going to use to derive meaning"""
        if isinstance(tree_node, str):
            return self.function_mappings.get(tree_node) or self.function_mappings.get("DEFAULT")
        else:
            # Token types in the AST
            return self.move_to_next

    def visit_program(self):
        node = Node(f"temp_var_{self.var_index}", 0)
        self.increment_var_index()
        self.move_to_next()
        returned_node = self.traverse()
        node.value = returned_node.value
        self.stmt_outputs.append(node)
        self.move_to_next(position="<stmt-list>")
        self.traverse()
        return node

    def visit_stmt_list(self):
        node = Node(f"temp_var_{self.var_index}", 0)
        self.increment_var_index()
        self.move_to_next()
        returned_node = self.traverse()
        node.value = returned_node.value
        self.stmt_outputs.append(node)
        self.move_to_next(position="<stmt-list>")
        self.traverse()
        return node

    def visit_stmt(self):
        node = Node(f"temp_var_{self.var_index}", 0)
        self.increment_var_index()
        returned_node = self.move_to_next()
        tree_node = self.get_tree_node(self.ast_index)
        if tree_node == ";":
            return returned_node
        else:
            saved_ast_index = self.ast_index
            # first get the variable declarations then execute the statements
            self.move_to_next(position="<declarations>")
            self.traverse()

            # next execute the statements
            self.ast_index = saved_ast_index
            self.move_to_next(position="EXP")
            tree_node = self.get_tree_node(self.ast_index)
            returned_node = self.traverse()

            node.value = returned_node.value
            return node

    def visit_declarations(self):
        node = Node(f"temp_var_{self.var_index}", 0)
        self.increment_var_index()
        self.move_to_next()
        tree_node = self.get_tree_node(self.ast_index)
        returned_node = self.traverse()
        if tree_node == ",":
            self.move_to_next(position="<var-dec>")
            self.traverse()
        return node

    def visit_var_dec(self):
        node = Node(f"temp_var_{self.var_index}", 0)
        self.increment_var_index()
        self.move_to_next(2)
        tree_node = self.get_tree_node(self.ast_index)
        variable_name = tree_node.token_value
        self.move_to_next(position="EXP")
        value_node = self.traverse()
        self.program_vars[variable_name] = value_node.value
        self.intermediate_code.extend([
            f"{variable_name}=[]",
            f"{variable_name}[0]={value_node.value}"
        ])
        self.move_to_next(position="<more-var-dec>")
        more_vars = self.traverse()
        return node

    def visit_more_var_dec(self):
        node = Node(f"temp_var_{self.var_index}", 0)
        self.increment_var_index()
        self.move_to_next()
        tree_node = self.get_tree_node(self.ast_index)
        if tree_node == "$":
            return node
        else:
            self.move_to_next(position="<var-dec>")
            node.value = self.traverse().value
        return node

    def visit_exp(self):
        node = Node(f"temp_var_{self.var_index}", 0)
        self.increment_var_index()
        returned_node = self.move_to_next()
        tree_node = self.get_tree_node(self.ast_index)
        if tree_node == "(":
            self.move_to_next(position="EXP")

        left_side_node = self.traverse()
        self.move_to_next(position="EXP2")
        right_side_node = self.traverse()
        if right_side_node.operator != "null":
            node.value = OPERATORS[right_side_node.operator](
                float(left_side_node.value), float(right_side_node.value)
            )
            self.intermediate_code.append(f"temp_var_{self.var_index}={left_side_node.value}{right_side_node.operator}{right_side_node.value}")
        else:
            node.value = left_side_node.value
            self.intermediate_code.append(f"temp_var_{self.var_index}={left_side_node.value}")
        return node

    def visit_ternary(self):
        node = Node(f"temp_var_{self.var_index}", 0)
        self.increment_var_index()
        self.move_to_next(position="<condition>")
        condition_node = self.traverse()
        self.move_to_next(position="<truthy>")
        truthy_node = self.traverse()
        self.move_to_next(position="<falsey>")
        falsey_node = self.traverse()

        node.value = float(truthy_node.value) if condition_node.value else float(falsey_node.value)

        return node

    def visit_exp2(self):
        node = Node(f"temp_var_{self.var_index}", 0)
        self.increment_var_index()
        self.move_to_next()
        operator = self.get_tree_node(self.ast_index)
        if operator not in BINARY_OPERATORS:
            return node
        self.move_to_next(position="EXP")
        exp = self.traverse()
        node.value = exp.value
        node.operator = operator
        return node

    def visit_name(self):
        node = Node(f"temp_var_{self.var_index}", 0)
        self.increment_var_index()
        self.move_to_next()
        token = self.get_tree_node(self.ast_index)
        node.name = token.token_value
        node.value = self.program_vars.get(token.token_value)
        if node.value is None:
            raise SemanticsError(
                f"Trying to access variable {node.name} before it has been declared."
            )
        self.intermediate_code.append(
            f"{token.token_value}={token.token_value}[0]"
        )
        return node

    def visit_number(self):
        node = Node(f"temp_var_{self.var_index}", 0)
        self.increment_var_index()
        self.move_to_next()
        token = self.get_tree_node(self.ast_index)
        node.value = token.token_value
        self.intermediate_code.append(
            f"temp_var{self.var_index}={token.token_value}"
        )
        return node
