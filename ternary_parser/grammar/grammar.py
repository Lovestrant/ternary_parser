"""
The LL(1) grammar

GRAMMAR {
    NON_TERMINAL : {
        "LOOKUP_TOKEN": [ REPLACEMENT GRAMMAR RULE ]
    }
}

The DEFAULT key is used to match every_other token that is not null
.
"""
from dataclasses import dataclass
from typing import Set

START = "PROGRAM"

DEFAULT = "DEFAULT"

END = "$"

NON_TERMINAL = set([
    START, "<stmt-list>", "STMT", "<declarations>", "<var-dec>", "<more-var-dec>", "EXP",
    "<ternary>", "<condition>", "<truthy>", "<falsey>", "EXP2",
])

BINARY_OPERATORS = set([
    "==", ">=", "<", "<=", "<", "+", "-", "*", "/",
])

KEYWORDS = set([
    "run", "if"
])

TERMINAL = set([
    END, ";", ",", ":", "if", "run", "+", "-", "*", "/", "(", ")",
    "{", "}", "`", "==", ">=", "<", "<=", "<", "NAME", "NUMBER"
])

GRAMMAR ={
    "PROGRAM": {
        DEFAULT: ["<stmt-list>", "PROGRAM"],
        END: [END],
    },
    "<stmt-list>": {
        DEFAULT: ["STMT", "<stmt-list>"],
        END: [END],
    },
    "STMT": {
        "run": ["run", "(", "`", "EXP", "`", "<declarations>", ")", ";"],
        ";": [";"],
    },
    "<declarations>": {
        ",": [",", "{", "<var-dec>", "}",],
        DEFAULT: [END],
    },
    "<var-dec>": {
        "NAME": ["NAME", ":", "EXP", "<more-var-dec>"],
    },
    "<more-var-dec>": {
        ",": [",", "<var-dec>"],
        DEFAULT: [END],
    },
    "EXP": {
        "NAME": ["NAME", "EXP2"],
        "NUMBER": ["NUMBER", "EXP2"],
        "if": ["<ternary>", "EXP2"],
        "(": ["(", "EXP", ")", "EXP2"],
    },
    "<ternary>": {
        "if": ["if", "(", "<condition>", ",", "<truthy>", ",", "<falsey>", ")"],
    },

    # basically just a copy of the EXP
    "<condition>": {
        "NAME": ["NAME", "EXP2"],
        "NUMBER": ["NUMBER", "EXP2"],
        "if": ["<ternary>", "EXP2"],
        "(": ["(", "EXP", ")", "EXP2"],
    },
    "<truthy>": {
        "NAME": ["NAME", "EXP2"],
        "NUMBER": ["NUMBER", "EXP2"],
        "if": ["<ternary>", "EXP2"],
        "(": ["(", "EXP", ")", "EXP2"],
    },
    "<falsey>": {
        "NAME": ["NAME", "EXP2"],
        "NUMBER": ["NUMBER", "EXP2"],
        "if": ["<ternary>", "EXP2"],
        "(": ["(", "EXP", ")", "EXP2"],
    },
    ####################################
    "EXP2":{
        "==": ["==", "EXP"],
        ">=": [">=", "EXP"],
        ">": [">", "EXP"],
        "<=": ["<=", "EXP"],
        "<": ["<", "EXP"],
        "+": ["+", "EXP"],
        "-": ["-", "EXP"],
        "*": ["*", "EXP"],
        "/": ["/", "EXP"],
        DEFAULT: [END],
    }
}

@dataclass(kw_only=True)
class Lexeme:
    __slots__= ['non_terminal', 'terminal', 'start', 'end', 'default']
    
    non_terminal: Set[str]
    terminal: Set[str]
    start: str
    end: str
    default: str
