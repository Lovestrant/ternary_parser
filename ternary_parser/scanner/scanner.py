import tokenize

class Scanner:
    """The lexer class of MiniTernary."""

    def __init__(self, file_path):
        """Identify the tokens from the source file using python's tokenize generator library module."""

        token_list = []

        with tokenize.open(file_path) as f:
            tokens = tokenize.generate_tokens(f.readline)
            token_list = [token for token in tokens]

        self.token_list = self.sanitize_tokens(token_list)

    def sanitize_tokens(self, token_list):
        """
        This takes the token list generated and adapts it to fit our MiniTernary Grammar.
        
        The token list generated after reading a file written in MiniTernary Grammar is basically pythonic tokens.
        
        This function serves to bring those pythonic tokens into the context of MiniTernary.
        """

        token_list = self.remove_comments(token_list)
        token_list = self.remove_unwanted_tokens(token_list)

        return token_list

    def remove_comments(self, token_list):
        """This method identifies comments /*...*/ and removes them."""

        comment_section = False
        remove_indices = []

        for i, token in enumerate(token_list):
            if token.string == "*":
                previous_index = i - 1
                next_index = i + 1

                # get comment start
                if comment_section is False:
                    if previous_index >= 0 and token_list[previous_index].string == "/":
                        comment_section = True
                        remove_indices.extend([previous_index, i])
                        continue

                # get comment end
                if comment_section is True:
                    if next_index < len(token_list) and token_list[next_index].string == "/":
                        comment_section = False
                        remove_indices.extend([i, next_index])
                        continue

            if comment_section is True:
                remove_indices.append(i)
        
        token_list[:] = [token for i, token in enumerate(token_list) if i not in remove_indices]

        return token_list

    def remove_unwanted_tokens(self, token_list):
        """Removes unwanted tokens."""

        tokens_to_remove = [
            "\n",
            "\t",
        ]
        remove_indices = []

        for i, token in enumerate(token_list):
            if token.string in tokens_to_remove:
                remove_indices.append(i)

        token_list[:] = [token for i, token in enumerate(token_list) if i not in remove_indices]

        return token_list
