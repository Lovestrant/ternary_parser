import argparse
import copy
from pathlib import Path
import token

from ternary_parser.parser.parser import (
    Parser,
    ParsingError,
    Token,
)
from ternary_parser.scanner import Scanner
from ternary_parser.grammar.grammar import (
    DEFAULT, END, GRAMMAR, Lexeme, NON_TERMINAL, START, TERMINAL
)
from ternary_parser.parser.interpreter import ASTVisitor

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-f", "--file", help="the absolute path of the source file to parse", required=False)
    args = parser.parse_args()

    # default file to compile if no file was specified
    if not args.file:
        project_dir = str(Path(__file__).parent.absolute())
        default_sample_file_path = project_dir + "/ternary_parser/tests/sample.mt"
        args.file = default_sample_file_path


    # scanning
    lexer = Scanner(args.file)
    token_list = copy.deepcopy(lexer.token_list)
    symbol_table = [
        Token(token_info, token.tok_name[token_info.type], token_info.string)
        for token_info in token_list
    ]

    # preparation for semantic analysis
    lexemes = Lexeme(
        non_terminal=NON_TERMINAL,
        terminal=TERMINAL,
        start=START,
        end=END,
        default=DEFAULT
    )

    # Actual parsing
    tern_parser = Parser(symbol_table, lexemes)
    tern_parser.parse()

    print("\n\nAST: ", tern_parser.AST)

    # Semantic Analysis and interpreting
    print("\n\n\nSEMANTIC ANALYSIS\n\n\n")
    interpreter = ASTVisitor(tern_parser.AST)
    computed= interpreter.traverse()

    print("\n\nVARIABLES: ", interpreter.program_vars)


    print("intermediate:", interpreter.intermediate_code)

    print("Stmt_outputs: ", interpreter.stmt_outputs)

    # The program output
    print("\n\n PROGRAM OUTPUT \n\n")
    for i in range(len(interpreter.stmt_outputs) - 2):
        print (f"stmt_{i}: {interpreter.stmt_outputs[i].value}")
