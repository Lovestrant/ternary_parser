## RUN

from the root folder:
```
python run.py    # this will run the sample input file by default

python run.py --file <absolute_file_path>  # this is used to parse an arbitrary file
```

## Checklist
- [x] Defining the grammar of mini_ternary

- [x] Lexing - Identifying tokens correctly, cleaning unusable tokens (comments etc)

- [x] Syntax Analysis - Identifying grammar errors
- [x] Semantic Analysis - Traversing the AST through recursive descent parsing and generating the different meanings 
## Todo (not yet implemented, but is planned)

- [ ] The grammar was not designed with the correct standard order of operations. Hence +, -, * and all other operations have the same precedence and are evaluated from left to right unless the order is specified using brackets. i.e  8 + 16 / 8 - 2 evaluates as 10.66 instead of 8.0 

The correct grammar that should be used:
````
```
expr        ::= comparison ((PLUS | MINUS) comparison)*

comparison  ::= term ((EQ | NEQ | LT | LTE | GT | GTE) term)*

term        ::= factor ((MUL | DIV) factor)*

factor      ::= INTEGER
               | LPAREN expr RPAREN
               | IDENTIFIER
               
```
````

## Docs
[grammar definition](https://docs.google.com/document/d/1FHEDorG1-NmGIuR6s498kmw8CFX5FWQlxQRCuTQ34B0/edit?usp=sharing)